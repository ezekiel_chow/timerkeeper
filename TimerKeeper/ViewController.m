//
//  ViewController.m
//  TimerKeeper
//
//  Created by Ezekiel Chow on 9/29/15.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#import "ViewController.h"
#import "MZTimerLabel.h"
#import <CRToast/CRToast.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface ViewController ()
{
    MZTimerLabel *stopwatch;
    int stopwatchTime;
    NSString *startTime, *endTime, *startDate, *endDate;
    BOOL isPause;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //declaration for class variable
    stopwatch = [[MZTimerLabel alloc]initWithLabel:_countUpLabel];
    stopwatchTime = 0;
    isPause = false;
    
    //make check out button invisible
    [_checkOutButton setAlpha:0.0f];
    [_checkOutButton setEnabled:false];
    
    //update the time n date
    [self populateDate];
    [self updateTime];
    
    //chcek 4 plist
    NSFileManager *fileManger=[NSFileManager defaultManager];
    NSError *error;
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    
    NSString *doumentDirectoryPath=[pathsArray objectAtIndex:0];
    
    NSString *destinationPath= [doumentDirectoryPath stringByAppendingPathComponent:@"TimeRecords.plist"];
    
    NSLog(@"plist path %@",destinationPath);
    if ([fileManger fileExistsAtPath:destinationPath]){
        //NSLog(@"database localtion %@",destinationPath);
        return;
    }
    NSString *sourcePath=[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:@"TimeRecords.plist"];
                          
    [fileManger copyItemAtPath:sourcePath toPath:destinationPath error:&error];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PopulateDateAndTime
-(void)populateDate
{
    //date
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *todayDate = [formatter stringFromDate:[NSDate date]];
    
    [_dateLabel setText:todayDate];
}

-(void)updateTime
{
    //time
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterMediumStyle];
    NSString *currentTime = [formatter stringFromDate:today];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_timeLabel setText:currentTime];
    });
    
    [self performSelector:@selector(updateTime) withObject:nil afterDelay:1];
}

#pragma mark - Check In
- (IBAction)checkInClicked:(id)sender
{
    if (!isPause)
    {
        NSDate *today = [NSDate date];
        NSDateFormatter *format = [[NSDateFormatter alloc]init];
        [format setDateStyle:NSDateFormatterMediumStyle];
        [format setTimeStyle:NSDateFormatterNoStyle];
        startDate = [format stringFromDate:today];
        [format setTimeStyle:NSDateFormatterMediumStyle];
        [format setDateStyle:NSDateFormatterNoStyle];
        startTime = [format stringFromDate:today];
        NSLog(@"start record %@ %@", startDate, startTime);
        
        [self fadeOutButton:_checkInButton fadeInButton:_checkOutButton];
    }
    else
    {
        [self fadeOutButton:_checkInButton fadeInButton:_pauseButton];
    }
    
    [stopwatch start];
}

/*
- (void)updateCountUpTimer:(NSNumber *)_timeInSeconds
{
    int timeToCountUp = [_timeInSeconds intValue];
    timeToCountUp++;
    int hours = timeToCountUp/3600;
    int minutes = (timeToCountUp%3600)/60;
    int seconds = timeToCountUp%60;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_countUpLabel setText:[NSString stringWithFormat:@"%d:%02d:%02d", hours, minutes, seconds]];
    });
    
    [self performSelector:@selector(updateCountUpTimer:) withObject:[NSNumber numberWithInt:timeToCountUp] afterDelay:1];
}
*/

#pragma mark - Pause
- (IBAction)pauseClicked:(id)sender
{
    [stopwatch pause];
    isPause = true;
    [self fadeOutButton:_pauseButton fadeInButton:_checkInButton];
}

#pragma mark - Check Out
- (IBAction)checkOutClicked:(id)sender
{
    NSDate *today = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc]init];
    [format setDateStyle:NSDateFormatterMediumStyle];
    [format setTimeStyle:NSDateFormatterNoStyle];
    endDate = [format stringFromDate:today];
    [format setTimeStyle:NSDateFormatterMediumStyle];
    [format setDateStyle:NSDateFormatterNoStyle];
    endTime = [format stringFromDate:today];
    NSLog(@"end record %@ %@", endDate, endTime);
    
    [stopwatch pause];
    stopwatchTime = [stopwatch getTimeCounted];
    NSLog(@"stopwatch time = %@", [[NSString alloc]initWithFormat:@"%d", stopwatchTime]);
    
    UIAlertView *saveTime = [[UIAlertView alloc]initWithTitle:@"Save Event" message:[[NSString alloc]initWithFormat:@"%d seconds", stopwatchTime] delegate:self cancelButtonTitle:@"Save" otherButtonTitles:@"Back", nil];
    saveTime.tag = 1;
    [saveTime setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [saveTime show];
}


#pragma mark - Animation
-(void)fadeOutButton:(UIButton *)buttonOut fadeInButton:(UIButton *)buttonIn
{
    [UIView animateWithDuration:0.3f animations:^{
        CABasicAnimation *rotationAnimation = [self rotateAnticlockwise:false];
        [buttonOut.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
        [buttonOut setEnabled:false];
        [buttonOut setAlpha:0.0f];
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3f animations:^{
            CABasicAnimation *rotationAnimation = [self rotateAnticlockwise:false];
            [buttonIn.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
            [buttonIn setAlpha:1.0f];
            [buttonIn setEnabled:true];
        }];
    }];
    
}

-(CABasicAnimation *)rotateAnticlockwise:(BOOL)direction
{
    float rotation;
    if(direction == false)
    {
        rotation = 2.0;
    }
    else
    {
        rotation = -2.0;
    }
    
    float duration = 1.0f, rotations = 2.0f;
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * rotation /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 1.0;
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    return rotationAnimation;
}

- (IBAction)historyClicked:(id)sender {
}
#pragma mark - Delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1)
    {
        if (buttonIndex == 0) //Saved pressed
        {
            [stopwatch reset];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
            // get documents path
            NSString *documentsPath = [paths objectAtIndex:0];
            // get the path to our Data/plist file
            NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"TimeRecords.plist"];
            NSLog(@"path %@", plistPath);
            
            NSMutableDictionary *dictMutable = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
            
            NSMutableArray *checkInTimeRecord = [dictMutable objectForKey:@"CheckInTime"];
            NSMutableArray *checkOutTimeRecord = [dictMutable objectForKey:@"CheckOutTime"];
            NSMutableArray *activityRecord = [dictMutable objectForKey:@"Activity"];
            NSMutableArray *checkInDateRecord = [dictMutable objectForKey:@"CheckInDate"];
            NSMutableArray *checkOutDateRecord = [dictMutable objectForKey:@"CheckOutDate"];

            NSLog(@"check data keys  %@ %@ %@ %@ %@", checkInTimeRecord.description, checkOutTimeRecord.description, activityRecord.description, checkInDateRecord.description, checkOutDateRecord.description);
            NSLog(@"heck value %@", checkOutDateRecord.description);
            
            NSString *activityText = [[alertView textFieldAtIndex:0] text];
            if ([activityText  isEqual: @""]) {
                activityText = @"none";
            }
            
            [checkInTimeRecord addObject:startTime];
            [checkInDateRecord addObject:startDate];
            [activityRecord addObject:activityText];
            [checkOutTimeRecord addObject:endTime];
            [checkOutDateRecord addObject:endDate];
            NSLog(@"heck value %@", checkOutDateRecord.description);
            
            /*NSDictionary *updatedFile = [[NSDictionary alloc]initWithObjectsAndKeys:checkOutDateRecord, @"CheckOutDate",
                                                 checkInDateRecord, @"CheckInDate",
                                                 activityRecord, @"Activity",
                                                 checkInTimeRecord, @"CheckInTime",
                                                 checkOutTimeRecord, @"CheckOutTime", nil];
            [fullFile writeToFile:filePath atomically:YES]; */
            
            if ([dictMutable writeToFile:plistPath atomically:YES]) {
                NSDictionary *options = @{
                                          kCRToastTextKey : @"Successfully Saved",
                                          kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                                          kCRToastBackgroundColorKey : [UIColor greenColor],
                                          kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                                          kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                                          kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionLeft),
                                          kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionRight)
                                          };
                [CRToastManager showNotificationWithOptions:options completionBlock:^{
                    NSLog(@"Toast Completed");
                }];
            }
            else
            {
                NSDictionary *options = @{
                                          kCRToastTextKey : @"Error",
                                          kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                                          kCRToastBackgroundColorKey : [UIColor redColor],
                                          kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
                                          kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeSpring),
                                          kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionLeft),
                                          kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionRight)
                                          };
                [CRToastManager showNotificationWithOptions:options completionBlock:^{
                    NSLog(@"Toast Failed");
                }];
            }
            
            [self fadeOutButton:_checkOutButton fadeInButton:_checkInButton];
        }
        else //Back pressed
        {
            [stopwatch start];
        }
    }
}

@end

